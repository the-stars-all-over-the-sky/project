# project

## 介绍

| 语言 & 环境                     | 名称                  | 描述                                               |
|:---------------------------:|:-------------------:|:------------------------------------------------ |
| C；ubuntu; VScode                    | netradio            | **基于IPv4的流媒体广播项目(UDP):https://gitee.com/the-stars-all-over-the-sky/DIY_WEB/tree/master/NET_RADIO**                          |
| C；VScode                    | IM                  | **双端即时通讯软件:https://gitee.com/the-stars-all-over-the-sky/DIY_WEB/tree/master/IM**                                     |
| C；VScode                    | cJson               | **这是一个简单地基于C语言的JSON解析器，包含了基本的节点创建、解析、封装、输出等等功能:https://gitee.com/the-stars-all-over-the-sky/DIY_WEB/tree/master/JSON_PARSE_BY_C** |
| C；VScode                    | DIY_TCP/IP          | **手写TCP/IP协议栈:https://gitee.com/the-stars-all-over-the-sky/DIY_WEB/tree/master/DIY_TCP**                                  |
| C；ubuntu-wsl/Windows-CLion；VScode | ntp_diy       | **手写NTP客户端:https://gitee.com/the-stars-all-over-the-sky/DIY_WEB/tree/master/DIY_NTP**                                    |
| C；汇编；VScode                 | x86_protection_mode | **设计x86保护模式下:https://gitee.com/the-stars-all-over-the-sky/X86_PROTECTION_MODE**                                  |
| C；Visual Studio 2022；VScode | FAT32               | **FAT32文件系统: https://gitee.com/the-stars-all-over-the-sky/DIY_SYSTEM/tree/master/DIY_FAT32**                                    |
| C；ubuntu-wsl；qemu；VScode | x86_system_diy       | **手写x86操作系统: https://gitee.com/the-stars-all-over-the-sky/DIY_SYSTEM/tree/master/DIY_SYSTEM_X86**                                    |
| C；ubuntu-wsl；qemu；VScode | risc_v_diy       | **手写RISC-V模拟器: https://gitee.com/the-stars-all-over-the-sky/DIY_SYSTEM/tree/master/DIY_RISCV**                                    |
| C；ubuntu-wsl；qemu；VScode | DIY_RTOS | **手写RTOS: https://gitee.com/the-stars-all-over-the-sky/DIY_SYSTEM/tree/master/DIY_RTOS**                                    |
| C；ubuntu-wsl；VScode | DIY_TFTP| **手写TFTP: https://gitee.com/the-stars-all-over-the-sky/DIY_WEB/tree/master/DIY_TFTP**                                    |
| C；ubuntu-wsl；VScode | DIY_COMPILER | **手写C语言编译器:https://gitee.com/the-stars-all-over-the-sky/DIY_COMPILER**                                    |
